package com.pluskarma.model;

import lombok.*;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * инвайт
 * <br />
 */
@Slf4j
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "invite")
public class Invite extends BaseModel
{
	@Column(name = "content")
	private String content;
	@Column(name = "customer_name")
	private String customerName;
	@Column(name = "phone_or_email")
	private String phoneOrEmail;
	@Column(name = "customer_questions")
	private String customerQuestions;
	@Column(name = "type")
	private String type;
	@Column(name = "status")
	private String status;
	
}
