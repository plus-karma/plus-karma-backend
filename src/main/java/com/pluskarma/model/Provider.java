package com.pluskarma.model;

import lombok.*;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Класс провайдера - системы в которой люди оставляют отзывы
 * <br />
 * Это может быть Яндекс Организации, Яндекс Маркет, Google My Business, Facebook и др.
 * <br />
 */
@Slf4j
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "provider")
public class Provider extends BaseModel
{
	@Column(name = "name")
	String name;
	
	@Column(name = "is_active")
	boolean isActive;
	
	@Column(name = "icon")
	String icon;
	
	@Column(name = "background_color")
	String backgroundColor;
	
	@Column(name = "text_color")
	String textColor;
	
	@Column(name = "order_of_provider")
	Integer orderOfProvider;
	
	@Column(name = "url")
	String url;
}
