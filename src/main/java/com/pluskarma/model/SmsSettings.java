package com.pluskarma.model;

import lombok.*;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Slf4j
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "sms_settings")
public class SmsSettings extends BaseModel
{
	//Язык //todo может лучше langKey
	@Column(name = "lang")
	String lang; //en-US, ru-Ru
	
	//
	@Column(name = "lang_title")
	String langTitle;
	
	//содержимое
	@Column(name = "sms_content")
	String smsContent;
	
	//
	@Column(name = "fallback")
	String fallback;
	
	@Column(name = "customer_question")
	String customerQuestion;
}
