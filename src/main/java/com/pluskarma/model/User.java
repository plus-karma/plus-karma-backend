package com.pluskarma.model;

import lombok.*;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.Instant;

/**
 * Класс пользователя
 * <br />
 */
@Slf4j
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "karma_user")
public class User extends BaseModel
{
	private static final long serialVersionUID = 1L;
	
	//@NotNull
	//@Column(name = "settlement_account")
	String settlementAccount;
	
	//@NotNull
	//@Pattern(regexp = Constant.LOGIN_REGEX)
	//@Size(min = 1, max = 50)
	//@Column(length = 50, unique = true, nullable = false)
	private String login;
	
	//@JsonIgnore
	//@NotNull
	//@Size(min = 60, max = 60)
	//@Column(name = "password_hash", length = 60)
	private String password;
	
	//@Size(max = 50)
	//@Column(name = "first_name", length = 50)
	private String firstName;
	
	//@Size(max = 50)
	//@Column(name = "last_name", length = 50)
	private String lastName;
	
	//@Email
	//@Size(min = 5, max = 100)
	//@Column(length = 100, unique = true)
	private String email;
	
	//@NotNull
	//@Column(nullable = false)
	@Builder.Default
	private boolean activated = false;
	
	//@Size(min = 2, max = 6)
	//@Column(name = "lang_key", length = 6)
	private String langKey;
	
	//@Size(max = 256)
	//@Column(name = "image_url", length = 256)
	private String imageUrl;
	
	//@Size(max = 20)
	//@Column(name = "activation_key", length = 20)
	//@JsonIgnore
	private String activationKey;
	
	//@Size(max = 20)
	//@Column(name = "reset_key", length = 20)
	//@JsonIgnore
	private String resetKey;
	
	//@Column(name = "reset_date")
	@Builder.Default
	private Instant resetDate = null;
	
	//@NotNull
	//@Column(name = "phone_number")
	private String phoneNumer;
	
	//@NotNull
	//@Column(name = "legal_address")
	private String legalAddress;
	
	//@NotNull
	//@Column(name = "actual_physical_address")
	private String actualPhysicalAddress;
	
	//@NotNull
	//@Column(name = "taxpayer_identification_number")
	private String taxpayerIdentificationNumber;
	
	//Correspondent account - корреспондентский счет
	//@Column(name = "correspondent_account")
	private String correspondentAccount;
	
	
	//BIC - Bank Identifier Code: Банковский Идентификационный Код уникальный знак, служащий для идентификации сделок.
//	@NotNull
//	@Column(name = "bic")
	private String bic;
	
	
	//todo кажется у на уже есть director? и это поле нужно удалить?
	//Chief Executive Officer
//	@NotNull
//	@Column(name = "cio")
	private String cio;
	//=================================================================================
	
	
	/**
	 * ФормаСобственности (typeOfBusinessEntity)
	 * <br />
	 */
//	@NotNull
//	@Column(name = "type_of_business_entity")
	private String typeOfBusinessEntity;
	
	/**
	 * НазваниеПредприятия (companyName)
	 * <br />
	 */
//	@NotNull
//	@Column(name = "company_name")
	private String companyName;
	
	/**
	 * ТипНалогообложения (taxSystem)
	 * <br />
	 */
//	@NotNull
//	@Column(name = "tax_system")
	private String taxSystem;
	
	/**
	 * ФиоДиректора(directorFullName)
	 * <br />
	 */
//	@NotNull
//	@Column(name = "director_full_name")
	private String directorFullName;
	
	/**
	 * КонтактноеЛицо(contactPerson)
	 * <br />
	 */
//	@NotNull
//	@Column(name = "contact_person")
	private String contactPerson;
	
	/**
	 * КонтактныйТелефон(contactPhone)
	 * <br />
	 */
//	@NotNull
//	@Column(name = "contact_phone")
	private String contactPhone;
	
	/**
	 * МобильныйТелефон (mobilePhone)
	 * <br />
	 */
//	@NotNull
//	@Column(name = "mobile_phone")
	private String mobilePhone;
	
	/**
	 *  Огрн (generalStateRegistrationNumberOGRN)
	 *  <br />
	 */
//	@NotNull
//	@Column(name = "general_state_registration_number_ogrn")
	private String generalStateRegistrationNumberOGRN;
	
	/**
	 *   КПП (taxRegistrationReasonCodeKPP)
	 *   <br />
	 */
//	@NotNull
//	@Column(name = "tax_registration_reason_code_kpp")
	private String taxRegistrationReasonCodeKPP;
	
	/**
	 *   Банк (bank)
	 *   <br />
	 */
//	@NotNull
//	@Column(name = "bank")
	private String bank;
	
	
	//=================================================================================
//	@Column(name = "daily_review_digest")
	@Builder.Default
	private boolean dailyReviewDigest = false;
	
	
//	@Column(name = "real_time_review_notification")
	@Builder.Default
	private boolean realTimeReviewNotification = false;
	
	
//	@Column(name = "weekly_summary")
	@Builder.Default
	private boolean weeklySummary = false;
	
//	@Column(name = "unresponded_message_notification")
	@Builder.Default
	private boolean unrespondedMessageNotification = false;
}
