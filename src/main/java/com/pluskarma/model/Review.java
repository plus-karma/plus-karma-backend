package com.pluskarma.model;

import lombok.*;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.Instant;

@Slf4j
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "rewiev")
public class Review extends BaseModel
{
	@Column(name = "provider_id")
	private Long providerId;
	
	@Column(name = "rating")
	private Integer rating;
	
	@Column(name = "text")
	private String text;
	
	@Column(name = "date")
	private Instant date;
	
	@Column(name = "manager_id")
	private Long managerId;
	
	@Column(name = "customer_name")
	private String customerName;
}
