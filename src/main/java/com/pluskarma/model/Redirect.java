package com.pluskarma.model;

import com.pluskarma.constant.Constant;
import lombok.*;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.Size;

/**
 * таблица сопоставления коротких ссылок и полных URL
 * <br />
 * на которые будет перенаправлен пользователь
 * <br />
 * при открытии короткой ссылки.
 * <br />
 * Короткие ссылки предназначены для
 * <br />
 * добавления в sms-сообщения пользователям и т.д.
 * <br />
 */
@Slf4j
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "redirect")
public class Redirect extends BaseModel
{
	/**
	 * уникальный идентификатор делающий короткую ссылку уникальной
	 */
	@Size(min = Constant.SHORT_LINK_ID_LENGTH, max = Constant.SHORT_LINK_ID_LENGTH)
	@Column(name = "short_id", unique = true, nullable = false)
	private String shortId;
	
	/**
	 * URL на который должно быть перенаправлена ссылка
	 */
	@Column(name = "redirect_url")
	private String redirectUrl;
	
	/**
	 * статус - указывает на то был ли выполнен переход по ссылке и т.д.
	 */
	@Column(name = "status")
	private String status;
}
