package com.pluskarma.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.envers.Audited;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Instant;

/**
 * Базовый абстрактный класс для всех хранимых сущностей
 * <br />
 */
@Slf4j
@Data
@MappedSuperclass
@Audited
public abstract class BaseModel implements Serializable
{
	private static final long serialVersionUID = 1L;
	
	@Column(nullable = false,
			updatable = false,
			unique = true)
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long id;
	
	//счетчик кол-ва изменений записи
	@Version
	@Column(nullable = false)
	public Long version;
	
	@CreatedBy
	@Column(name = "created_by", nullable = false, length = 50, updatable = false)
	@JsonIgnore
	private String createdBy = "";
	
	@CreatedDate
	@Column(name = "created_date", nullable = false)
	@JsonIgnore
	private Instant createdDate = Instant.now();
	
	@LastModifiedBy
	@Column(name = "last_modified_by", length = 50)
	@JsonIgnore
	private String lastModifiedBy;
	
	@LastModifiedDate
	@Column(name = "last_modified_date")
	@JsonIgnore
	private Instant lastModifiedDate = Instant.now();
}
