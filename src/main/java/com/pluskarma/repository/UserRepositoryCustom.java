package com.pluskarma.repository;

import com.pluskarma.model.User;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepositoryCustom
{
	public void someCustomMethod(User user);
}
