package com.pluskarma.repository;

import com.pluskarma.model.Review;
import org.springframework.stereotype.Repository;

@Repository
public interface ReviewRepositoryCustom
{
	public void someCustomMethod(Review review);
}
