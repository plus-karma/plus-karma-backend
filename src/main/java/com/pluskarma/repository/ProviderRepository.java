package com.pluskarma.repository;

import com.pluskarma.model.Provider;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProviderRepository extends JpaRepository<Provider, Long>, ProviderRepositoryCustom
{

}
