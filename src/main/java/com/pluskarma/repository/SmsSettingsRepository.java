package com.pluskarma.repository;

import com.pluskarma.model.SmsSettings;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Repository;

@Repository
@RepositoryRestResource(path = "smsSettings", collectionResourceRel = "smsSettings")
public interface SmsSettingsRepository extends JpaRepository<SmsSettings, Long>, SmsSettingsRepositoryCustom
{

}
