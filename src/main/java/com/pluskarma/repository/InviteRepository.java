package com.pluskarma.repository;

import com.pluskarma.model.Invite;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InviteRepository extends JpaRepository<Invite, Long>, InviteRepositoryCustom
{
	// Declare query methods here
}

