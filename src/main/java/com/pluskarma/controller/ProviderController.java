package com.pluskarma.controller;

import com.pluskarma.model.Provider;
import com.pluskarma.repository.ProviderRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.BasePathAwareController;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 *
 */
@Slf4j
@BasePathAwareController
public class ProviderController
{
	@Autowired
	ProviderRepository providerRepository;
	
	@RequestMapping(
			value = "providers/{id}",
			method = RequestMethod.PUT,
			produces="application/hal+json")
	public @ResponseBody ResponseEntity<?> saveProvider(@RequestBody Provider sourceProvider, @PathVariable("id") Long id)
	{
		Provider targetProvider = providerRepository.findOne(id);
		
		/*
		 Копируем все свойства (кроме тех, что равны null и кроме id-свойства)
		 из объекта с обновлениями,
		 в объект прочитанный нами только что из бд.
		 После этого сохраняем обновленный объект в базе данных.
		 */
		BeanUtils.copyProperties(sourceProvider, targetProvider, PropertiesHelper.getNullPropertyNames(sourceProvider));
		
		providerRepository.save(targetProvider); //save(merged)
		
		return ResponseEntity.ok(targetProvider);
	}
}
