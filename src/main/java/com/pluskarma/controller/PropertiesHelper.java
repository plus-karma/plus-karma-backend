package com.pluskarma.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;

import java.beans.FeatureDescriptor;
import java.util.stream.Stream;

/**
 *
 */
@Slf4j
public class PropertiesHelper
{
	/**
	 * Метод используется для копирования свойств объектов
	 * <br />
	 * Объект1 содержит все ствойства среди которых некоторое
	 * <br />
	 * подмножество содержит значения для обновления
	 * <br />
	 * а некоторые установлены в null
	 * <br />
	 * Объект2 хранится в базе данных и должен быть обновлен,
	 * <br />
	 * в таком случае мы должны использовать для обновления только те значениями из Объекта1,
	 * <br />
	 * которые не равны null, (т.к. в противном случае в нашем объекте будет обновлено одно поле,
	 * <br />
	 * которое мы изначально и хотели обновить, а остальные поля будут затерты null-лами,
	 * <br />
	 * чего мы делать не хотим).
	 * <br />
	 * Метод позволяет получить для объекта список его свойств имеющих значение null,
	 * <br />
	 * Для удобства в список свойств также добавлено поле id
	 * <br />
	 * @param source - исходный объект
	 * @return - массив содержащий строки с именами свойств
	 */
	public static String[] getNullPropertyNames(Object source)
	{
		final BeanWrapper wrappedSource = new BeanWrapperImpl(source);
		return Stream.concat(Stream.of(wrappedSource.getPropertyDescriptors())
						.map(FeatureDescriptor::getName)
						.filter(propertyName -> wrappedSource.getPropertyValue(propertyName) == null)
				, Stream.of("id"))
				
				.toArray(String[]::new);
	}
	
}
