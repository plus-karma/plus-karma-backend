package com.pluskarma.controller.redirect;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

/**
 *
 */
@Controller
@RequestMapping("/redirect")
public class RedirectController
{
	@GetMapping("/{id}")
	public RedirectView redirectWithUsingRedirectView(RedirectAttributes attributes, @PathVariable("id") String id)
	{
		//System.out.println("redirect : " + id);
		//attributes.addFlashAttribute("flashAttribute", "redirectWithRedirectView");
		//attributes.addAttribute("attribute", "redirectWithRedirectView");
		return new RedirectView("/api/users/" + id);
	}
}
