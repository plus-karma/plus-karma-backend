package com.pluskarma.trash;

import com.pluskarma.model.BaseModel;

//Отзывы
// класс для обработки каждого из отзывов
public class ReviewIssue extends BaseModel
{
	//номер i
	Long id;
	//компания о которой оставлен отзыв
	Long Company;
	//человек, который оставил отзыв
	Long Reporter;
	//сервис на котором отзыв был оставлен
	Long ReporterService;
	//сотрудник, который должен обработать отзыв
	Long Assignee;
	//тип отзыва
	Long IssueType;
	//краткое описание отзыва
	String Summary;
	//расширенное содержимое отзыва
	String Description;
	//статус обработки отзыва
	Long IssueStatus;
	//Отзывы
	Long Opinion;
	//рабочий процесс по которому обрабатываются отзывы
	Long WorkflowId;
	
	//todo нужна ли история изменения отзыва? т.к пользователь может свои отзывы редактировать
}
