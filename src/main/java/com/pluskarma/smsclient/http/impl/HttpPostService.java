package com.pluskarma.smsclient.http.impl;

import com.pluskarma.smsclient.http.HttpServiceException;
import com.pluskarma.smsclient.http.beans.HttpServiceResponse;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.IOException;

public class HttpPostService extends HttpBaseService
{
	
	public HttpPostService(String host, String query)
	{
		super(host, query);
	}
	
	public HttpPostService(String host)
	{
		super(host);
	}
	
	@Override
	public HttpServiceResponse execute() throws HttpServiceException
	{
		//todo deprecated
		HttpClient httpClient = new DefaultHttpClient();
		HttpPost httpPost = createHttpPost();
		
		try
		{
			HttpResponse httpResponse = httpClient.execute(httpPost);
			HttpServiceResponse response = new HttpServiceResponse(httpResponse);
			
			return response;
		} catch (IOException e)
		{
			throw new HttpServiceException(e);
		} finally
		{
			httpPost.releaseConnection();
		}
	}
	
	private HttpPost createHttpPost()
	{
		String url = getUrlBuilder().getUrl();
		
		HttpPost httpPost = new HttpPost(url);
		//todo charset Добавил, но похоже это никакого особого результата при отправке смс не даёт. Потому может стоит удалить обратно?
		httpPost.addHeader("Content-Type", "application/x-www-form-urlencoded; charset=utf-8");
		
		return httpPost;
	}
}
