package com.pluskarma.smsclient.http;

import com.pluskarma.smsclient.http.beans.HttpServiceResponse;

public interface HttpService
{
	
	public void addParameter(String paramName, String paramValue);
	
	public void addParameter(String paramName, Integer paramValue);
	
	public void addParameters(String paramName, String... paramValue);
	
	public HttpServiceResponse execute() throws HttpServiceException;
}
