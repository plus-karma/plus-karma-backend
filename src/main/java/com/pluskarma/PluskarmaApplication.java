package com.pluskarma;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@EnableAutoConfiguration
@SpringBootApplication
public class PluskarmaApplication
{
	public static void main(String[] args)
	{
		SpringApplication.run(PluskarmaApplication.class, args);
	}
}
