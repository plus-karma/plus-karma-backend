package com.pluskarma.event_handlers;

import com.pluskarma.model.Invite;
import com.pluskarma.repository.InviteRepository;
import com.pluskarma.repository.SmsSettingsRepository;
import com.pluskarma.smsclient.main.SmsSender;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.core.annotation.HandleAfterCreate;
import org.springframework.data.rest.core.annotation.RepositoryEventHandler;

/**
 *
 */
@Slf4j
@RepositoryEventHandler
public class InviteEventHandler
{
	@Autowired
	InviteRepository inviteRepository;
	@Autowired
	SmsSettingsRepository smsSettingsRepository;
	
	@HandleAfterCreate //Create работает для POST, Save для PUT
	public void handleBeforeCreated(Invite invite)
	{
		log.info("Обрабатываем инвайт, сообщение отправлено на номер " + invite.getPhoneOrEmail());
		
		invite.setCustomerQuestions(
				smsSettingsRepository.findOne(1L).
						getCustomerQuestion());
		String smsContent = smsSettingsRepository.findOne(1L).getSmsContent();
		String shortLink = "http://52.166.1.70:8888/#/r/" +  invite.getId();
		
		SmsSender.createClientAndSendSms(invite.getPhoneOrEmail(), smsContent + " " + shortLink);
		
		invite.setStatus("sending");
		inviteRepository.saveAndFlush(invite);
	}
}
