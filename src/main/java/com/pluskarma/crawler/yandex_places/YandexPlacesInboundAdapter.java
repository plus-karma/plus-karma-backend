package com.pluskarma.crawler.yandex_places;

import com.pluskarma.crawler.InboundAdapter;
import com.pluskarma.crawler.MySettings;
import com.pluskarma.model.Review;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.invoke.MethodHandles;
import java.util.List;

/**
 *
 */
public class YandexPlacesInboundAdapter implements InboundAdapter
{
	private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
	
	public static void main(String[] args)
	{
		test();
	}
	
	private static void test()
	{
		YandexPlacesInboundAdapter adapter = new YandexPlacesInboundAdapter();
		List<Review> reviews = adapter.getReviews("");
		
	}
	
	@Override
	public List<Review> getReviews(String companyId)
	{
		try
		{
			MySettings.ProxySettings proxySettings = MySettings.proxySettingsList.get(0);
			String proxyUser = proxySettings.user;
			String proxyPassword = proxySettings.pass;
			int proxyPort = proxySettings.port;
			String proxyHost = proxySettings.host;
			
			String PROXY = proxyHost + ":" +  proxyPort;
			
			org.openqa.selenium.Proxy proxy = new org.openqa.selenium.Proxy();
			proxy.setSocksUsername(proxyUser);
			proxy.setSocksPassword(proxyPassword);
			
			proxy.setHttpProxy(PROXY)
					.setFtpProxy(PROXY)
					.setSslProxy(PROXY);
			DesiredCapabilities cap = new DesiredCapabilities();
			cap.setCapability(CapabilityType.PROXY, proxy);
			
			HtmlUnitDriver driver = new HtmlUnitDriver();
			driver.setJavascriptEnabled(true);
			
			
			driver.get("https://yandex.ua/maps/org/agentstvo_internet_reklamy_i_media/1020979294/?reviews");
			//driver.get("https://ya.ru");
			WebElement elementByClassName = driver.findElementByClassName("section-review-tex");
			System.out.println(elementByClassName.getText());
			
		}
		finally
		{
		
		}
		return null;
		
	}
}
