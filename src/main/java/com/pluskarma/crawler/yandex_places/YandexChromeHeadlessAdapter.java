package com.pluskarma.crawler.yandex_places;

import com.google.common.io.Resources;
import com.pluskarma.crawler.MySettings;
import com.pluskarma.utils.FileAppender;
import com.pluskarma.utils.TextReplacer;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.invoke.MethodHandles;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import static com.pluskarma.utils.ChromePluginManager.makePluginFilesListForProxyPlugin;
import static com.pluskarma.utils.ZipManager.makeZip;

/**
 *
 */
public class YandexChromeHeadlessAdapter
{
	private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
	
	public static void main(String[] args) throws IOException, InterruptedException
	{
		//получаем временный файл background.js для плагина
		// (файл содержит вместо настроек прокси - переменные для шаблонизатора)
		Path path = Paths.get(Resources.getResource("proxy/plugin_sources/background.js").getPath());
		
		//заменяем переменные настройками прокси и получаем новый временный файл
		String backgroudJsFullName = TextReplacer.replaseProxyParamatersForFile(path);
		
		//формируем список файлов для упаковки
		List<Path> pathList = makePluginFilesListForProxyPlugin(backgroudJsFullName);
		
		//формируем временный файл плагина
		String fullZipFileName = makeZip(pathList, "proxy_auth.zip");
		
		//парсим Яндекс
		parseYandex(fullZipFileName);
	}
	
	private static void parseYandex(String pluginZipFileName) throws IOException, InterruptedException
	{
		
		try
		{
			MySettings.ProxySettings proxySettings = MySettings.proxySettingsList.get(4);
			
			Map<String, String> hashMap = new HashMap<String, String>()
			{
				{
					//put("proxy", "http://user:password@proxy_url:port");
					put("--proxy-server", proxySettings.allParamLink);
					System.out.println(proxySettings.allParamLink);
				}
			};
			ChromeDriverService service = new ChromeDriverService.Builder()
					.usingDriverExecutable(new File("/home/algernon/IdeaProjects/plus-karma-backend/src/main/resources/drivers/chromedriver"))
					.usingAnyFreePort()
					.withEnvironment(hashMap)
					.build();
			
			service.start();
			
			Proxy proxy = new Proxy();
			proxy.setHttpProxy(proxySettings.hostAndPort);
			proxy.setSslProxy(proxySettings.hostAndPort);
			//proxy.setSocksUsername(proxySettings.user);
			//proxy.setSocksPassword(proxySettings.pass);
			
			DesiredCapabilities capabilities = DesiredCapabilities.chrome();
			//capabilities.setCapability(CapabilityType.PROXY, proxy);
			
			ChromeOptions options = new ChromeOptions();
			options.addExtensions(new File(pluginZipFileName));
			
			//options.addArguments("--headless"); //  # Runs Chrome in headless mode.
			//options.addArguments("disable-gpu"); //# Temporarily needed if running on Windows.
			
			capabilities.setCapability(ChromeOptions.CAPABILITY, options);
			
			WebDriver driver = new RemoteWebDriver(service.getUrl(), capabilities);
			
			driver.manage().window().maximize();
			
			driver.get("https://yandex.ru/maps/org/agentstvo_internet_reklamy_i_media/1020979294/?reviews");
			driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			
			WebElement buttonSizeXl = driver.findElement(By.className("button_size_xl"));
			while (!buttonSizeXl.getText().equals(""))
			{
				buttonSizeXl.click();
				buttonSizeXl = driver.findElement(By.className("button_size_xl"));
			}
			
			List<WebElement> reviews = driver.findElements(By.className("review"));
			
			FileAppender fileAppender = new FileAppender();
			for (WebElement rev : reviews)
			{
				WebElement revDescription = rev.findElement(By.className("review__description"));
				String attribute = revDescription.getAttribute("innerHTML");
				
				if (driver instanceof JavascriptExecutor)
				{
					Object result = ((JavascriptExecutor) driver)
							.executeScript("return arguments[0].innerHTML", revDescription);
					fileAppender.writer.write("\n-------------------------------------------\n");
					fileAppender.writer.write(String.valueOf(result));
					fileAppender.writer.write("\n-------------------------------------------\n");
				}
			}
			fileAppender.writer.close();
			
			driver.manage().timeouts().implicitlyWait(25, TimeUnit.SECONDS);
		}
		catch (Exception ex)
		{
			StringWriter stringWriter = new StringWriter();
			PrintWriter writer = new PrintWriter(stringWriter);
			ex.printStackTrace(writer);
			log.error(stringWriter.toString());
		}
	}
}
