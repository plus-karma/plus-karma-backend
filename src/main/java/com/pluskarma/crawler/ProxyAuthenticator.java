package com.pluskarma.crawler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.invoke.MethodHandles;
import java.net.Authenticator;
import java.net.PasswordAuthentication;

/**
 *
 */
public class ProxyAuthenticator extends Authenticator
{
	private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
	
	private PasswordAuthentication auth;
	
	protected ProxyAuthenticator(String user, String password)
	{
		auth = new PasswordAuthentication(user, password == null ? new char[]{} : password.toCharArray());
	}
	
	protected PasswordAuthentication getPasswordAuthentication() {
		return auth;
	}
	
}
