package com.pluskarma.crawler;

import com.pluskarma.model.Review;

import java.util.List;

/**
 *
 */
public interface InboundAdapter
{
	//todo добавить компанию в модели
	List<Review> getReviews(String companyId);
}
