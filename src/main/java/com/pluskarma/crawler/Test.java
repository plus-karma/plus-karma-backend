//package com.pluskarma.crawler;
//
//import okhttp3.OkHttpClient;
//import okhttp3.Request;
//import okhttp3.Response;
//import org.apache.commons.codec.binary.Base64;
//import org.jsoup.Jsoup;
//import org.jsoup.nodes.Document;
//import org.jsoup.nodes.Element;
//import org.jsoup.select.Elements;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//
//import java.io.IOException;
//import java.lang.invoke.MethodHandles;
//import java.net.Authenticator;
//import java.net.InetSocketAddress;
//import java.net.PasswordAuthentication;
//import java.net.Proxy;
//import java.util.concurrent.TimeUnit;
//
//
///**
// *
// */
//public class Test
//{
//	private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
//
//	public static void main(String[] args) throws IOException
//	{
//		String url = "https://yandex.ru/maps/org/agentstvo_internet_reklamy_i_media/1020979294/?reviews";
//
//		MySettings.ProxySettings settings = MySettings.proxySettingsList.get(2);
//
//		String proxyUser = settings.user;
//		String proxyPassword = settings.pass;
//		int proxyPort = settings.port;
//		String proxyHost = settings.host;
//
//
//		System.getProperties().put("http.proxyUser", proxyUser);
//		System.getProperties().put("http.proxyPassword", proxyPassword);
//		System.getProperties().put("https.proxyUser", proxyUser);
//		System.getProperties().put("https.proxyPassword", proxyPassword);
//
//
//		Authenticator.setDefault(new Authenticator() {
//			protected PasswordAuthentication getPasswordAuthentication() {
//
//				return new PasswordAuthentication(proxyUser, proxyPassword.toCharArray());
//			}
//		});
//
//		String encoded = new String(Base64.encodeBase64((proxyUser + ":" + proxyPassword).getBytes()));
//
//
//
//
//		Proxy proxyTest = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(proxyHost, proxyPort));
//
//
//		OkHttpClient.Builder builder = new OkHttpClient.Builder().proxy(proxyTest).connectTimeout(15, TimeUnit.SECONDS).readTimeout(15, TimeUnit.SECONDS)
//				.writeTimeout(15, TimeUnit.SECONDS);
//		OkHttpClient client = builder.build();
//
//
//
//
//		Request request = new Request.Builder()
//				.url(url)
//				//.header("Proxy-Authorization", "Basic " + encoded)
//				//.header("Proxy-Authorization", Credentials.basic(proxyUser, proxyPassword))
//				.build();
//
//
//
//		Response response = client.newCall(request).execute();
//
//		String html = response.body().string();
//		System.out.println(html);
//		Document doc = Jsoup.parse(html);
//		//Elements reviews = doc.getElementsByClass("review__description");
//		Elements reviews = doc.getElementsByClass("button_size_xl");
//		for(int i = 0; i < reviews.size(); i++)
//		{
//			Element rev = reviews.get(i);
//			System.out.println("-------------------------------------");
//			System.out.println(rev.text());
//			System.out.println("-------------------------------------");
//		}
//	}
//
//}
