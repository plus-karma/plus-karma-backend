package com.pluskarma.crawler;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class MySettings
{
	/**
	 * список из объектов
	 * <br />
	 * каждый объект содержит настройки прокси-сервера
	 */
	public static List<ProxySettings> proxySettingsList;
	static
	{
		proxySettingsList = new ArrayList<>();
		proxySettingsList.add(new ProxySettings("46.73.33.253", 8080, "", ""));
		proxySettingsList.add(new ProxySettings("196.202.230.36", 8080, "", ""));
		proxySettingsList.add(new ProxySettings("41.203.183.50", 8080, "", ""));
		proxySettingsList.add(new ProxySettings("109.185.180.87", 8080, "", ""));
		proxySettingsList.add(new ProxySettings("demi4.com", 3128, "vv", "Rfhfrfnbwf123"));
	}
	
	/**
	 * список из объектов
	 * <br />
	 * каждый объект содержит настройки для оптравки SMS
	 */
	public static List<SmsApiSettings> smsApiSettingsList;
	static
	{
		smsApiSettingsList = new ArrayList<>();
		smsApiSettingsList.add(new SmsApiSettings("ivanfil", "97Redroses", "DTSMS"));
		smsApiSettingsList.add(new SmsApiSettings("testkabinet", "123456", "Agentstvo"));
	}
	
	
	
	/**
	 * Настройки прокси серверов
	 * <br />
	 */
	public static class ProxySettings
	{
		public final String host;
		public final int port;
		public final String user;
		public final String pass;
		public final String hostAndPort;
		public final String allParamLink;
		
		public ProxySettings(String host, int port, String user, String pass)
		{
			this.host = host;
			this.port = port;
			this.user = user;
			this.pass = pass;
			this.hostAndPort = host + ":" + port;
			this.allParamLink = "http://" + user + ":" + pass + "@" + host + ":" + port;
		}
	}
	
	/**
	 * Настройки различных провайдеров СМИ
	 * <br />
	 * или разных тестовых кабинетов одного и того же провайдера
	 */
	public static class SmsApiSettings
	{
		public final String user;
		public final String pass;
		public final String alphaName;
		
		public SmsApiSettings(String user, String pass, String alphaName)
		{
			this.user = user;
			this.pass = pass;
			this.alphaName = alphaName;
		}
	}
	
	/**
	 * телефоны которые я использую для тестирования отправки СМС
	 * <br />
	 */
	public class Phones
	{
		public final String phone;
		
		public Phones(String phone)
		{
			this.phone = phone;
		}
	}
	
}
