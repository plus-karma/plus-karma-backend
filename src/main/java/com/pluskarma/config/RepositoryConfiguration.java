package com.pluskarma.config;

import com.pluskarma.event_handlers.InviteEventHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 *
 */
@Slf4j
@Configuration
public class RepositoryConfiguration
{
	@Bean
	InviteEventHandler inviteEventHandler()
	{
		return new InviteEventHandler();
	}
}
