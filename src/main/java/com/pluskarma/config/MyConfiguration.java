package com.pluskarma.config;

import com.pluskarma.model.*;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurerAdapter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;


@Configuration
public class MyConfiguration
{
	
	@Bean
	public FilterRegistrationBean corsFilter()
	{
		UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		CorsConfiguration config = new CorsConfiguration();
		config.setAllowCredentials(true);
		config.addAllowedOrigin("*");
		config.addAllowedHeader("*");
		config.addAllowedMethod("*");
		source.registerCorsConfiguration("/**", config);
		FilterRegistrationBean bean = new FilterRegistrationBean(new CorsFilter(source));
		bean.setOrder(0);
		return bean;
	}
	
	@Bean
	public RepositoryRestConfigurerAdapter repositoryRestConfigurerAdapter()
	{
		return new RepositoryRestConfigurerAdapter()
		{
			/**
			 * Exposing ID for some entities
			 */
			@Override
			public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config)
			{
				config.exposeIdsFor(User.class);
				config.exposeIdsFor(SmsSettings.class);
				config.exposeIdsFor(Review.class);
				config.exposeIdsFor(Provider.class);
				config.exposeIdsFor(Invite.class);
				
				config.setBasePath("/api");
				super.configureRepositoryRestConfiguration(config);
			}
			
		};
	}
}
