package com.pluskarma.populate;

import com.pluskarma.model.Invite;
import com.pluskarma.repository.InviteRepository;
//import com.pluskarma.repository.InviteRepository;

public class InvitePopulateHelper
{
	public static void populate(
			InviteRepository inviteRepository
	)
	{
		inviteRepository.save(Invite.builder()
				.content("some content1")
				.customerName("Alex Petrov1")
				.phoneOrEmail("+585633254125")
				.customerQuestions("Hello?1")
				.build());
		
		inviteRepository.save(Invite.builder()
				.content("some content2")
				.customerName("Vanya Ivanov2")
				.phoneOrEmail("+380665418941")
				.customerQuestions("Hello?2")
				.build());
		
		//todo delete me
		inviteRepository.someCustomMethod(Invite.builder().build());
		
	}
}
