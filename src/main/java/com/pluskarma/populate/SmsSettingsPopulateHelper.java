package com.pluskarma.populate;

import com.pluskarma.model.SmsSettings;
import com.pluskarma.repository.SmsSettingsRepository;

public class SmsSettingsPopulateHelper
{
	public static void populate(SmsSettingsRepository smsSettingsRepository)
	{
		smsSettingsRepository.save(SmsSettings.builder()
				.lang("RU")
				.langTitle("Русский")
				.smsContent("Вы не могли бы оставить отзыв о нашей компании?")
				.fallback("fallback1")
				.customerQuestion("Вам понравилось?|Да|Нет")
				.build());
		
		smsSettingsRepository.save(SmsSettings.builder()
				.lang("RU")
				.langTitle("Русский")
				.smsContent("Вы не могли бы оставить отзыв о компании Google?")
				.fallback("fallback2")
				.customerQuestion("Вам понравилось?|Да|Нет")
				.build());
	}
}
