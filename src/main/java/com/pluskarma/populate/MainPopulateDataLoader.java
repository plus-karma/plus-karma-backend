package com.pluskarma.populate;

import com.pluskarma.repository.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

/**
 *
 */
@Slf4j
@Component
public class MainPopulateDataLoader implements CommandLineRunner
{
	
	private final UserRepository userRepository;
	private final ReviewRepository reviewRepository;
	private final ProviderRepository providerRepository;
	private final SmsSettingsRepository smsSettingsRepository;
	private final InviteRepository inviteRepository;
	
	
	@Autowired
	public MainPopulateDataLoader(UserRepository userRepository,
	                              ReviewRepository reviewRepository,
	                              ProviderRepository providerRepository,
	                              SmsSettingsRepository smsSettingsRepository
			, InviteRepository inviteRepository
	)
	{
		this.userRepository = userRepository;
		this.reviewRepository = reviewRepository;
		this.providerRepository = providerRepository;
		this.smsSettingsRepository = smsSettingsRepository;
		this.inviteRepository = inviteRepository;
	}
	
	@Override
	public void run(String... strings) throws Exception
	{
		
		UserPopulateHelper.populate(this.userRepository);
		ReviewPopulateHelper.populate(this.reviewRepository);
		ProviderPopulateHelper.populate(this.providerRepository);
		SmsSettingsPopulateHelper.populate(this.smsSettingsRepository);
		InvitePopulateHelper.populate(this.inviteRepository);
	}
}
