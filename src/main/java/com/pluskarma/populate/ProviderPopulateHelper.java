package com.pluskarma.populate;

import com.pluskarma.model.Provider;
import com.pluskarma.repository.ProviderRepository;

public class ProviderPopulateHelper
{
	public static void populate(ProviderRepository providerRepository)
	{
		providerRepository.save(Provider.builder()
				.name("Facebook")
				.isActive(true)
				.icon("unknown")
				.backgroundColor("#49689F")
				.textColor("#fff")
				.orderOfProvider(1)
				.url("http://wikipedia.com")
				.build());
		
		providerRepository.save(Provider.builder()
				.name("Google")
				.isActive(true)
				.icon("unknown")
				.backgroundColor("#3E9CF3")
				.textColor("#fff")
				.orderOfProvider(2)
				.url("http://wikipedia.com")
				.build());
		
		providerRepository.save(Provider.builder()
				.name("Yandex")
				.isActive(true)
				.icon("unknown")
				.backgroundColor("#FAD30A")
				.textColor("#000")
				.orderOfProvider(3)
				.url("http://yandex.ru")
				.build());
		
		providerRepository.save(Provider.builder()
				.name("Cars.com")
				.isActive(false)
				.icon("unknown")
				.backgroundColor("#4B2079")
				.textColor("#fff")
				.orderOfProvider(2)
				.url("http://wikipedia.com")
				.build());
		
		providerRepository.save(Provider.builder()
				.name("G2 Growd")
				.isActive(false)
				.icon("unknown")
				.backgroundColor("#415D6B")
				.textColor("#fff")
				.orderOfProvider(1)
				.url("http://wikipedia.com")
				.build());
	}
}
