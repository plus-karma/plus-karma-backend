package com.pluskarma.populate;

import com.pluskarma.model.User;
import com.pluskarma.repository.UserRepository;

import java.time.Instant;

public class UserPopulateHelper
{
	public static void populate(UserRepository userRepository)
	{
		
		userRepository.save(User.builder()
				.login("proxy/plugin_sources")
				.password("proxy/plugin_sources")
				.firstName("Alex")
				.lastName("Petrov")
				.email("petrov@gmail.com")
				.activated(true)
				.langKey("ru")
				.imageUrl("imageUrl1")
				.activationKey("activationKey1")
				.resetKey("resetKey1")
				.resetDate(Instant.now())
				.phoneNumer("phoneNumer1")
				.legalAddress("legalAddress1")
				.actualPhysicalAddress("actualPhysicalAddress1")
				.taxpayerIdentificationNumber("taxpayerIdentificationNumber1")
				.settlementAccount("settlementAccount1")
				.correspondentAccount("correspondentAccount1")
				.bic("bic1")
				.cio("cio1")
				.dailyReviewDigest(true)
				.realTimeReviewNotification(true)
				.weeklySummary(true)
				.unrespondedMessageNotification(true)
				.typeOfBusinessEntity("typeOfBusinessEntity1")
				.companyName("companyName1")
				.taxSystem("taxSystem1")
				.directorFullName("directorFullName1")
				.contactPerson("contactPerson1")
				.contactPhone("contactPhone1")
				.mobilePhone("mobilePhone1")
				.generalStateRegistrationNumberOGRN("generalStateRegistrationNumberOGRN1")
				.taxRegistrationReasonCodeKPP("taxRegistrationReasonCodeKPP1")
				.bank("bank1")
				.build());
		
		userRepository.save(User.builder()
				.login("proxy/etc")
				.password("proxy/etc")
				.firstName("Petr")
				.lastName("Ivanov")
				.email("Ivanov@gmail.com")
				.activated(true)
				.langKey("ru")
				.imageUrl("imageUrl2")
				.activationKey("activationKey2")
				.resetKey("resetKey2")
				.resetDate(Instant.now())
				.phoneNumer("phoneNumer2")
				.legalAddress("legalAddress2")
				.actualPhysicalAddress("actualPhysicalAddress2")
				.taxpayerIdentificationNumber("taxpayerIdentificationNumber2")
				.settlementAccount("settlementAccount2")
				.correspondentAccount("correspondentAccount2")
				.bic("bic2")
				.cio("cio2")
				.dailyReviewDigest(true)
				.realTimeReviewNotification(true)
				.weeklySummary(true)
				.unrespondedMessageNotification(true)
				.typeOfBusinessEntity("typeOfBusinessEntity2")
				.companyName("companyName2")
				.taxSystem("taxSystem2")
				.directorFullName("directorFullName2")
				.contactPerson("contactPerson2")
				.contactPhone("contactPhone2")
				.mobilePhone("mobilePhone2")
				.generalStateRegistrationNumberOGRN("generalStateRegistrationNumberOGRN2")
				.taxRegistrationReasonCodeKPP("taxRegistrationReasonCodeKPP2")
				.bank("bank2")
				.build());

	}
}
