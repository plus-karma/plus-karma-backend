package com.pluskarma.populate;

import com.pluskarma.model.Review;
import com.pluskarma.repository.ReviewRepository;

import java.time.Instant;

public class ReviewPopulateHelper
{
	public static void populate(ReviewRepository reviewRepository)
	{
		reviewRepository.save(Review.builder().
				providerId(1L)
				.rating(5)
				.text("все очень понравилось")
				.date(Instant.now())
				.managerId(1L)
				.build());
		
		reviewRepository.save(Review.builder().
				providerId(2L)
				.rating(-5)
				.text("фигня какая-то")
				.date(Instant.now())
				.managerId(2L)
				.build());
		
		reviewRepository.someCustomMethod(Review.builder().build());
	}
}
