package com.pluskarma.constant;

import lombok.Getter;
import lombok.Setter;

/**
 * имена переменных которые используются в шаблонах
 * <br />
 */
public enum VarsOfTemplates
{
	CUSTOMER_NAME("customer_name"),
	EMAIL("email"),
	PHONE_NUMBER("phone_number"),
	LOCATION_NAME("location_name");
	
	@Getter
	@Setter
	private String var;
	
	private VarsOfTemplates(String var)
	{
		this.var = var;
	}
}
