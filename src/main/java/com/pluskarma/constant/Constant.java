package com.pluskarma.constant;

import lombok.extern.slf4j.Slf4j;

/**
 * Константы, используемые в приложении
 * <br />
 */
@Slf4j
public final class Constant
{
	// Regex for acceptable logins
	public static final String LOGIN_REGEX = "^[_'.A-Za-z0-9-]*$";
	
	public static final String SYSTEM_ACCOUNT = "system";
	public static final String ANONYMOUS_USER = "anonymoususer";
	public static final String DEFAULT_LANGUAGE = "ru";
	public static final int SHORT_LINK_ID_LENGTH = 6;
	
	private Constant()
	{
	
	}
}
