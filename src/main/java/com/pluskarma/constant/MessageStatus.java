package com.pluskarma.constant;

import lombok.Getter;
import lombok.Setter;

/**
 * Статусы отправляемых сообщений
 * <br />
 */
public enum MessageStatus
{
	ACCEPTED("accepted"),
	SENT("sent"),
	DELIVERED("delivered"),
	FAILED("failed"),
	UNDELIVERED("undelivered");
	
	@Getter
	@Setter
	private String status;
	
	private MessageStatus(String status)
	{
		this.status = status;
	}
}
