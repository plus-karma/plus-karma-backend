package com.pluskarma.constant;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 *
 */
@ToString
public enum MessageType
{
	EMAIL("E-mail"), SMS("SMS");
	
	@Getter
	@Setter
	private String type;
	
	private MessageType(String type)
	{
		this.type = type;
	}
}
