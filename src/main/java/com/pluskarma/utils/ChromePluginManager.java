package com.pluskarma.utils;

import com.google.common.io.Resources;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.invoke.MethodHandles;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class ChromePluginManager
{
	private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
	
	public static void main(String[] args)
	{
		makePluginFilesListForProxyPlugin(Resources.getResource("proxy/plugin_sources/background.js").getFile());
	}
	
	public static List<Path> makePluginFilesListForProxyPlugin(String backgroundJsFullFileName)
	{
		Path path1 = Paths.get(backgroundJsFullFileName);
		Path path2 = Paths.get(Resources.getResource("proxy/plugin_sources/manifest.json").getFile());
		
		List<Path> filesList = new ArrayList<Path>()
		{
			{
				add(path1);
				add(path2);
			}
		};
		return filesList;
	}
}
