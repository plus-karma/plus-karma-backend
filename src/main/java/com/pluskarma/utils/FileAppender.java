package com.pluskarma.utils;

import lombok.extern.slf4j.Slf4j;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 *
 */
@Slf4j
public class FileAppender
{
	public BufferedWriter writer;
	
	public FileAppender()
	{
		try
		{
			this.writer = Files.newBufferedWriter(Paths.get("result.txt"));
		}
		catch (IOException ex)
		{
			log.error(ex.getMessage());
		}
	}
}
