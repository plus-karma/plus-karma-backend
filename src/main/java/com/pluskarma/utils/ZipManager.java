package com.pluskarma.utils;

import com.google.common.io.Resources;
import org.hibernate.validator.constraints.NotEmpty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.invoke.MethodHandles;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * Класс для управления Zip-архивами
 * <br />
 */
public class ZipManager
{
	private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
	
	public static void main(String[] args)
	{
		Path path1 = Paths.get(Resources.getResource("proxy/plugin_sources/manifest.json").getFile());
		Path path2 = Paths.get(Resources.getResource("proxy/plugin_sources/background.js").getFile());
		
		List<Path> filesList = new ArrayList<Path>()
		{
			{
				add(path1);
				add(path2);
			}
		};
		
		makeZip(filesList, "myZip.zip");
	}
	
	/**
	 * передаем список файлов,
	 * <br />
	 * получаем архив в который упакованы все эти файлы
	 * <br />
	 * Архив помещается во временную директорию и временный файл.
	 * <br />
	 * @param filesList - список передаваемых для упаковки файлов
	 * @param archiveNameWithExtension - имя архива (также необходимо указать в имени расширение .zip)
	 * @return имя созданного файла-архива
	 */
	public static String makeZip(List<Path> filesList, @NotEmpty String archiveNameWithExtension)
	{
		String resultZipFilename = "";
		FileInputStream fin = null;
		ZipOutputStream zipOut = null;
		try
		{
			resultZipFilename = TempFilesManager.createTempFileName(archiveNameWithExtension);
			FileOutputStream fileOutputStream = new FileOutputStream(resultZipFilename);
			
			zipOut = new ZipOutputStream(fileOutputStream);
			
			for (Path path : filesList)
			{
				ZipEntry entry = new ZipEntry(path.getFileName().toString());
				
				fin = new FileInputStream(path.toFile());
				
				byte[] b = new byte[1024];
				zipOut.putNextEntry(entry);
				int length;
				while ((length = fin.read(b, 0, 1024)) > 0)
				{
					zipOut.write(b, 0, length);
				}
			}
		}
		catch (IOException ex)
		{
			log.error(ex.getMessage());
		}
		finally
		{
			if (fin != null)
			{
				try
				{
					fin.close();
					zipOut.close();
				}
				catch (IOException ex)
				{
					log.error(ex.getMessage());
				}
			}
		}
		
		//имя созданного файла-архива
		return resultZipFilename;
	}
}
