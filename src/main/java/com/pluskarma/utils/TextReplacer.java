package com.pluskarma.utils;

import com.google.common.io.Resources;
import com.pluskarma.crawler.MySettings;
import lombok.extern.slf4j.Slf4j;
import org.stringtemplate.v4.ST;
import org.stringtemplate.v4.STErrorListener;
import org.stringtemplate.v4.misc.ErrorBuffer;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 *
 */
@Slf4j
public class TextReplacer
{
	//todo !
	public static void main(String[] args) throws IOException, URISyntaxException
	{
		Path path = Paths.get(Resources.getResource("proxy/plugin_sources/background.js").getPath());
		String newFile = replaseProxyParamatersForFile(path);
		log.info(newFile);
	}
	
	/**
	 * Читаем файл, заменяем в нем параметры прокси,
	 * <br />
	 * создаем и записываем новый файл с установленными значениями параметров прокси
	 * <br />
	 * @param path
	 * @return
	 */
	public static String replaseProxyParamatersForFile(Path path)
	{
		String tempFileName = "";
		try
		{
			String sourceString = new String(Files.readAllBytes(path));
			ST template = new ST(sourceString);

			MySettings.ProxySettings proxySettings = MySettings.proxySettingsList.get(4);

			template.add("proxy_host", proxySettings.host);
			template.add("proxy_port", proxySettings.port);
			template.add("username", proxySettings.user);
			template.add("password", proxySettings.pass);
			template.add("all_urls", "<all_urls>"); //todo я тут мы пересекаемся с синтаксисом шаблонов

			//создаём временный файл
			tempFileName = TempFilesManager.createTempFileName("background.js");
			STErrorListener errors = new ErrorBuffer();

			template.write(new File(tempFileName), errors);
			log.debug("{}", template.render());
		}
		catch (IOException ex)
		{
			log.error(ex.getMessage());
		}
		
		return tempFileName;
	}
}
