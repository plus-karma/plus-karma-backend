package com.pluskarma.utils;

import com.google.common.io.Files;
import lombok.extern.slf4j.Slf4j;

import java.io.File;

/**
 *
 */
@Slf4j
public class TempFilesManager
{
	public static void main(String[] args)
	{
		createTempFileName("example.txt");
	}
	
	public static String createTempFileName(String fileName)
	{
		File myTempDir = Files.createTempDir();
		log.info("Create temp dir {}", myTempDir);
		String resultFileName = myTempDir.getPath() + File.separator + fileName;
		log.info("generate full path to file {}", resultFileName);
		return resultFileName;
	}
}
